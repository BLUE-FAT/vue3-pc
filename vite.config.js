import { defineConfig } from "vite";
import { injectHtml } from "vite-plugin-html";
import { resolve } from "path";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";

const pkg = require("./package.json");

export default defineConfig({
  base: "./",
  server: {
    port: 4500,
    open: true,
    cors: true,
    proxy: {
      "/api": {
        target: "http://xxx.xxx.xxx.xxx:x000",
        changeOrigin: true,
        secure: false
        // rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  plugins: [
    vue(),
    vueJsx(),
    injectHtml({
      injectData: {
        version: pkg.version,
        updated_time: new Date().toLocaleString()
      }
    })
  ],
  resolve: {
    alias: [{ find: "@", replacement: resolve(__dirname, "src") }]
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@use "@/assets/css/common.scss" as *; `
      }
    }
  }
});
