import "@/assets/js/utils/rem.js"; // 若需要适配需要
import "./router/permission";
import "@/assets/css/element-variables.scss";
import "@/assets/css/global.scss";

import * as icons from "@element-plus/icons-vue";

import App from "./App.vue";
import ElementPlus from "element-plus";
import { createApp } from "vue";
import { global } from "@/assets/js/globalConfig.js";
import permission from "@/assets/js/directive/permission";
import router from "./router";
import store from "./store";

const app = createApp(App);

Object.keys(icons).forEach(key => {
  app.component(key, icons[key]);
});
app.config.globalProperties._global = global; // 引入全局变量
app.config.globalProperties.BASE_API = import.meta.env.VITE_APP_BASE_API;

app.use(router).use(store).use(ElementPlus).use(permission).mount("#app");
