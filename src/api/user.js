import axios from "axios";
import request from "@/assets/js/utils/request";

export const sys = null; // 认证服务系统代号

// token续期  key:refreshToken
export function getRefreshToken(data) {
  return axios({
    url: "/api/v1/sys/refreshToken",
    method: "post",
    data
  });
}

// 登录

// 微信登录基本信息
export function getWxInfo() {
  return request({
    url: "/api/v1/sys/wx/appInfo",
    method: "get"
  });
}

// 微信登录 key: code state loginType
// 短信登录 key: phone phoneCaptcha loginType
export function login(data) {
  return request({
    url: "/api/v1/sys/login",
    method: "post",
    data: { ...data, sys }
  });
}
// 获取验证码
export function phoneCode(phone) {
  return request({
    url: "/api/v1/sys/phone_capcha",
    method: "get",
    params: { phone }
  });
}
// 获取验证码（登录后）
export function phoneCodeLogged() {
  return request({
    url: "/api/v1/sys/capcha",
    method: "get"
  });
}
// 修改手机号（当前用户、绑定新号码） key:phone phoneCapcha
export function changePhone(data) {
  return request({
    url: "/api/v1/sys/user/phone",
    method: "post",
    data
  });
}

/**
 * 个人资料相关
 */

// 当前用户信息
export function getInfo() {
  return request({
    url: "/api/v1/sys/user/current",
    method: "get"
  });
}
// 修改个人资料信息
export function modificationInfo(data1, data2) {
  return request({
    url: "/api/v1/sys/self/wrapper",
    method: "post",
    data: {
      user: { ...data1, sys },
      userExtension: data2 // 拓展信息
    }
  });
}

// 禁用单个用户（管理员）
export function disableUser(id) {
  return request({
    url: `/api/v1/sys/user/${id}/disable`,
    method: "post"
  });
}
// 启用单个用户（管理员）
export function enableUser(id) {
  return request({
    url: `/api/v1/sys/user/${id}/enable`,
    method: "post"
  });
}

/**
 * END
 */
