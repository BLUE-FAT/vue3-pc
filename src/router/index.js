import { createRouter as Router, createWebHashHistory } from "vue-router";

export const constantRoutes = [
  {
    path: "/",
    name: "homepage",
    component: () => import("@/views/Homepage.vue")
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/",
    meta: {
      title: "NotFound",
      hidden: true
    }
  }
];

export const asyncRoutes = [];

const createRouter = () =>
  Router({
    history: createWebHashHistory(),
    routes: constantRoutes,
    scrollBehavior: () => ({ left: 0, top: 0 })
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
