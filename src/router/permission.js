import { ElMessage } from "element-plus";
import nprogress from "nprogress";
import router from "./index";
import store from "@/store";

nprogress.configure({ showSpinner: false });
const whiteList = ["/login", "/auth", "/"];

router.beforeEach(async (to, from, next) => {
  nprogress.start();
  const hasToken = store.getters.token;
  if (hasToken) {
    const { roles } = store.getters;
    if (to.path === "/login") {
      next({
        path: "/"
      });
    } else if (to.path === "/register") {
      // 注册页面误入判断
      if (Array.isArray(roles) && roles.length > 0) {
        return next({
          path: "/"
        });
      }
      next();
    } else if (to.path === "/choose-roles") {
      // 选择权限页面误入判断
      if (Array.isArray(roles) && roles.length < 2) {
        return next({
          path: "/"
        });
      }
      next();
    } else if (Array.isArray(roles) && roles.length === 1) {
      next();
    } else if (Array.isArray(roles) && roles.length > 1) {
      return next({
        path: "/choose-roles"
      });
    } else {
      try {
        const userInfo = await store.dispatch("user/getInfo");
        const accessRoutes = await store.dispatch("permission/generateRoutes", userInfo.user.roleKeys);
        accessRoutes.forEach(route => {
          router.addRoute(route);
        });
        next({ ...to, replace: true });
      } catch (error) {
        if (error === "no-rules") {
          next({
            path: "/register",
            query: {
              redirect: to.path,
              ...to.query
            }
          });
          return ElMessage({
            message: "新用户请先注册",
            type: "warning",
            duration: 5000
          });
        }
        console.log("error", error);
        await store.dispatch("user/resetToken");
        next({
          path: "/login",
          query: {
            redirect: to.path,
            ...to.query
          }
        });
      }
    }
  } else if (whiteList.indexOf(to.path) !== -1) {
    next();
  } else {
    next({
      path: "/login",
      query: {
        redirect: to.path,
        ...to.query
      }
    });
  }
});

router.afterEach(() => {
  nprogress.done();
});
