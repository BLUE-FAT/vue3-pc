module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ["plugin:vue/vue3-essential", "airbnb-base", "plugin:prettier/recommended"],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module"
  },
  plugins: ["html", "vue", "prettier"],
  rules: {
    "no-console": "off",
    "no-debugger": "off",
    "no-unused-vars": [
      "warn",
      {
        argsIgnorePattern: "^_"
      }
    ],
    "prettier/prettier": "warn",
    "arrow-parens": 0,
    avoidEscape: 0,
    "space-before-function-paren": 0,
    "generator-star-spacing": 0,
    semi: [0],
    indent: ["off", 2],
    "no-multi-spaces": "warn",
    "no-prototype-builtins": "warn",
    "no-undef": "warn",
    "prefer-const": "warn",
    "key-spacing": [
      0,
      {
        singleLine: {
          beforeColon: true,
          afterColon: true
        },
        multiLine: {
          beforeColon: true,
          afterColon: true,
          align: "colon"
        }
      }
    ],
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "import/no-absolute-path": "off",
    "import/no-extraneous-dependencies": "off",
    "vue/no-multiple-template-root": "off",
    "no-param-reassign": ["off", { props: false }],
    "consistent-return": "off",
    "no-underscore-dangle": "off",
    "no-shadow": "off",
    "prefer-promise-reject-errors": "off",
    "func-names": "off",
    "prefer-rest-params": "off",
    camelcase: "off",
    "no-plusplus": "off",
    radix: "off",
    "no-multi-assign": "off",
    "import/order": "off",
    "import/prefer-default-export": "off"
  }
};
